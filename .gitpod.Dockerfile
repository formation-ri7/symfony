FROM gitpod/workspace-full

RUN sudo install-packages php-intl php-redis php-amqp

RUN sudo apt update && sudo apt install -y apt-utils apt-transport-https

RUN curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.deb.sh' | sudo -E bash
RUN sudo apt install symfony-cli